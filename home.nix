{ pkgs, ... }:

{
  home.packages = with pkgs; [
    cmatrix
    wget
    kitty
    i3status-rust
    neofetch
    feh
    ranger
    syncthing
    syncthing-gtk
    tdesktop
    signal-desktop
    fish
    firefox
    light
    htop
    arc-theme
    discord
    skype
    zoom-us
    bpytop
    scrot
    fd
    ripgrep
    python
    nodejs
    cmake
    gnumake
    jq
    shellcheck
    go
    ccls
    gcc
    texlive.combined.scheme-full
    steam
  ];

  xdg.configFile = {
    "alacritty/".source = dotfiles/alacritty;
    "fish/".source = dotfiles/fish;
    "gtk-3.0/".source = dotfiles/gtk-3.0;
    "i3/".source = dotfiles/i3;
    "i3status-rust/".source = dotfiles/i3status-rust;
    "kitty/".source = dotfiles/kitty;
    "picom/".source = dotfiles/picom;
    "ranger/rc.conf".source = dotfiles/ranger/rc.conf;
  };

  home.file = {
    ".bash-powerline.sh".source = dotfiles/bash/.bash-powerline.sh;
    ".doom.d/".source = dotfiles/.doom.d;
  };

  programs.emacs = {
    enable = true;
    extraPackages = epkgs: [ epkgs.vterm ];
  };
  programs.bash.enable = true;
  programs.bash.initExtra = "source ~/.bash-powerline.sh";

  programs.home-manager = {
    enable = true;
    path = "/home/flygrounder";
  };

  programs.git = {
    enable = true;
    userEmail = "flygrounder@tutanota.com";
    userName = "Artyom Belousov";
  };

  services.emacs.enable = true;
  services.picom.enable = true;
}
