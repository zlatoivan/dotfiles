{ config, pkgs, ... }:

{
  imports =
    [
      ../modules/basic.nix
      ../modules/i3.nix
      ../modules/plasma.nix
      ../modules/hp-printer.nix
      ../modules/32-bit.nix
      ./hardware-configuration.nix
    ];

  networking.hostName = "desktop";

  users.users = {
    dmitry = {
      isNormalUser = true;
      extraGroups = [
        "networkmanager" # Networking
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    brave
    firefox
    libreoffice
  ];
}
