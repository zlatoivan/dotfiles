{ config, pkgs, ... }:

{
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = "experimental-features = nix-command flakes";
  };

  time.timeZone = "Europe/Moscow";

  networking.useDHCP = false;

  fonts.fonts = with pkgs; [
    roboto
    powerline-fonts
    font-awesome
    fira-code
    fira-code-symbols
  ];

  services.xserver.layout = "us,ru";
  services.xserver.xkbOptions = "grp:alt_shift_toggle";

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  users.users = {
    flygrounder = {
      isNormalUser = true;
      extraGroups = [
        "wheel" # Enable ‘sudo’ for the user.
        "networkmanager" # Networking
        "video"
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    networkmanager
    vim
    git
  ];

  nix.allowedUsers = [ "flygrounder" ];

  networking.networkmanager.enable = true;
  programs.nm-applet.enable = true;
  system.stateVersion = "20.09"; # Did you read the comment?

}

