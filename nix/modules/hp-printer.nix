{ config, pkgs, ... }:

{
  nixpkgs.config.allowUnfree = true;
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplipWithPlugin ];
  environment.systemPackages = with pkgs; [
    hplip
  ];
}
