{ config, pkgs, ... }:

{
  imports =
    [
      ../modules/basic.nix
      ../modules/i3.nix
      ../modules/touchpad.nix
      ./hardware-configuration.nix
    ];

  services.tlp.enable = true;
  networking.hostName = "laptop";

  services.xserver  = {
    displayManager = {
      defaultSession = "none+i3";
    };
  };

  programs.light.enable = true;
}
