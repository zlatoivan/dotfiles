# My dotfiles
This repository contains all my configs. They are not meant to be used as is, but may be useful as a reference.

|||
|-------------------|---------------|
| OS                | Nix OS        |
| Window manager    | i3-gaps            |
| Status bar        | i3status-rust |
| Terminal emulator | kitty         |
| Shell             | fish          |
| Shell prompt      | lucid.fish    |

![Screenshot](/images/Screenshot.png)

## Installation
You need to have [Nix](https://nixos.org/) and [home manager](https://github.com/nix-community/home-manager) installed.

``` shell
mkdir ~/.config
git clone https://gitlab.com/flygrounder/dotfiles.git .config/nixpkgs
home-manager switch
```
