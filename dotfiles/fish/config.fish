set fish_greeting
set PATH "$PATH:$HOME/.emacs.d/bin:$HOME/go/bin"

alias rb "sudo nixos-rebuild switch --flake /etc/nixos"
