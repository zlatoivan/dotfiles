alias ls='ls --color=auto'
alias ll='ls -la'
alias grep='grep --color=auto'

export HISTSIZE=1000
export HISTIGNORE="&:[bf]g:exit"

source ~/.bash-powerline.sh

export EDITOR="vim"
